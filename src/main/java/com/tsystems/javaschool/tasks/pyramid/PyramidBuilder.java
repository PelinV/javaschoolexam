package com.tsystems.javaschool.tasks.pyramid;

import java.util.List;

import java.util.Collections; // Because of sort()

import java.lang.Math; // Because of floor()

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */

    public int checkList(int inputNumbersSize) {
        for (int i = 0, j = 0; i <= inputNumbersSize; ++j, i+=j) {
            if (i == inputNumbersSize){
                return j;
            }
        }
        throw new CannotBuildPyramidException();
    }
    
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        // TODO : Implement your solution here
        if (inputNumbers.size() > 1000) {
            throw new CannotBuildPyramidException();
        }

        for (Integer inputNumber : inputNumbers) {
            if (inputNumber == null) {
                throw new CannotBuildPyramidException();
            }
        }


        Collections.sort(inputNumbers);

        int n = checkList(inputNumbers.size());
        int m = n*2 - 1;

        int[][] outputNumbers = new int [n][m];

        int idx = 0, startPosition;
        for (int i = 0; i < n; ++i) {
            startPosition = (int)(Math.floor(m/2) - i);
            for(int j = 0; j < i + 1; ++j){  
                outputNumbers[i][startPosition + 2*j] = inputNumbers.get(idx++);
            }
        }

        return outputNumbers;
    }


}
