package com.tsystems.javaschool.tasks.zones;

import java.util.List;

public class RouteChecker {


    /**
     * Checks whether required zones are connected with each other.
     * By connected we mean that there is a path from any zone to any zone from the requested list.
     *
     * Each zone from initial state may contain a list of it's neighbours. The link is defined as unidirectional,
     * but can be used as bidirectional.
     * For instance, if zone A is connected with B either:
     *  - A has link to B
     *  - OR B has a link to A
     *  - OR both of them have a link to each other
     *
     * @param zoneState current list of all available zones
     * @param requestedZoneIds zone IDs from request
     * @return true of zones are connected, false otherwise
     */

    public void dfs(int position, int size, boolean [] visited, boolean [][] half_graph){ // Depth-first search
        visited[position] = true;
        for (int i = 0; i < size; ++i) {
            if (half_graph[position][i] == true) {
                if (!visited[i]) {
                       dfs(i, size, visited, half_graph);
                }
            }
        }   
    }

    public boolean checkRoute(List<Zone> zoneState, List<Integer> requestedZoneIds){
        // TODO : Implement your solution here

        label: for (Integer requestedZoneId : requestedZoneIds) {
            for (Zone zone : zoneState) {
                if (requestedZoneId == zone.getId()) {
                    continue label;
                }
            }
            return false;
        }

        // This need to fix
        boolean [][] graph = new boolean [zoneState.size()][zoneState.size()];

        for (int i = 0; i < zoneState.size() ; ++i) {
            for (int j = 0; j < zoneState.get(i).getNeighbours().size(); ++j) {
                graph[i][zoneState.get(i).getNeighbours().get(j) - 1] = true;
                graph[zoneState.get(i).getNeighbours().get(j) - 1][i] = true;
            }
        }

        boolean [][] half_graph = new boolean [requestedZoneIds.size()][requestedZoneIds.size()];

        for (Integer i : requestedZoneIds) {
            for (Integer j : requestedZoneIds) {
                half_graph[requestedZoneIds.indexOf(i)][requestedZoneIds.indexOf(j)] = graph[i - 1][j - 1];
            }
        }

        boolean [] visited = new boolean [requestedZoneIds.size()];

        dfs(0, requestedZoneIds.size(), visited, half_graph);

        for (boolean flag : visited) {
            if (!flag) {
                return false;
            }
        }
        
        return true;
    }

}
